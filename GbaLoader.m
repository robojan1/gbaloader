//
//  GbaLoader.m
//  GbaLoader
//

#import "GbaLoader.h"

@implementation GbaLoader {
	NSObject<HPHopperServices> *_services;
}

+(int) sdkVersion {
	return HOPPER_CURRENT_SDK_VERSION;
}

- (instancetype)initWithHopperServices:(NSObject<HPHopperServices> *)services {
	if(self = [super init]) {
		_services = services;
	}
	return self;
}

- (HopperUUID *)pluginUUID {
    return [_services UUIDWithString:@"4a8f4b30-9e52-4931-a9db-52b68a4beb67"];
}

- (HopperPluginType)pluginType {
    return Plugin_Loader;
}


- (NSString *)pluginName {
    return @"GBA ROM";
}

- (NSString *)pluginDescription {
    return @"GBA ROM File Loader";
}

- (NSString *)pluginAuthor {
    return @"Robbert-Jan de Jager";
}

- (NSString *)pluginCopyright {
    return @"© Robbert-Jan de Jager";
}

- (NSString *)pluginVersion {
    return @"0.0.1";
}

- (NSString *)commandLineIdentifier {
    return @"GBA";
}

- (CPUEndianess)endianess {
    return CPUEndianess_Little;
}

- (BOOL)canLoadDebugFiles {
    return NO;
}

// Returns an array of DetectedFileType objects.
- (NSArray *)detectedTypesForData:(NSData *)data ofFileNamed:(NSString *)filename {
    if ([data length] < 4) return @[];

    NSObject<HPDetectedFileType> *type = [_services detectedType];
    [type setFileDescription:@"GBA ROM"];
    [type setAddressWidth:AW_32bits];
    [type setCpuFamily:@"arm"];
    [type setCpuSubFamily:@"v6"];
    [type setShortDescriptionString:@"gba"];

    if ([[filename pathExtension] isEqualToString:@"gba"]) {
    	[type setLowPriority:NO];
    } else {
    	[type setLowPriority:YES];
    }

    return @[type];
}


- (FileLoaderLoadingStatus)loadData:(NSData *)data usingDetectedFileType:(NSObject<HPDetectedFileType> *)fileType options:(FileLoaderOptions)options forFile:(NSObject<HPDisassembledFile> *)file usingCallback:(FileLoadingCallbackInfo)callback {
    uint32_t sizeInBytes = [data length];

    uint32_t firstAddress = 0x08000000;

    callback(@"Loading GBA ROM File", 0.0f);
    NSObject<HPSegment> *segment = [file addSegmentAt:firstAddress size:sizeInBytes];
    NSObject<HPSection> *section = [segment addSectionAt:firstAddress size:sizeInBytes];
    segment.segmentName = @"ROM";
    section.sectionName = @"ROM";
    section.containsCode = YES;
    segment.mappedData = data;
    segment.fileOffset = 0;
    segment.fileLength = sizeInBytes;
    section.fileOffset = 0;
    section.fileLength = sizeInBytes;

    segment = [file addSegmentAt:0x02000000 size:0x40000];
    section = [segment addSectionAt:0x02000000 size:0x40000];
    segment.segmentName = @"OBWRAM";
    section.sectionName = @"OBWRAM";
    section.zeroFillSection = YES;

    segment = [file addSegmentAt:0x03000000 size:0x8000];
    section = [segment addSectionAt:0x03000000 size:0x8000];
    segment.segmentName = @"OCWRAM";
    section.sectionName = @"OCWRAM";
    section.zeroFillSection = YES;

    segment = [file addSegmentAt:0x04000000 size:0x400];
    section = [segment addSectionAt:0x04000000 size:0x400];
    segment.segmentName = @"IO";
    section.sectionName = @"IO";
    section.pureDataSection = YES;

    segment = [file addSegmentAt:0x05000000 size:0x400];
    section = [segment addSectionAt:0x05000000 size:0x400];
    segment.segmentName = @"Palette";
    section.sectionName = @"Palette";
    section.zeroFillSection = YES;

    segment = [file addSegmentAt:0x06000000 size:0x18000];
    section = [segment addSectionAt:0x06000000 size:0x18000];
    segment.segmentName = @"VRAM";
    section.sectionName = @"VRAM";
    section.zeroFillSection = YES;

    segment = [file addSegmentAt:0x07000000 size:0x400];
    section = [segment addSectionAt:0x07000000 size:0x400];
    segment.segmentName = @"OAM";
    section.sectionName = @"OAM";
    section.zeroFillSection = YES;

    [file setName:@"DISPCNT" forVirtualAddress:0x04000000 reason:NCReason_Import];
    [file setName:@"GRNSWAP" forVirtualAddress:0x04000002 reason:NCReason_Import];
    [file setName:@"DISPSTAT" forVirtualAddress:0x04000004 reason:NCReason_Import];
    [file setName:@"VCOUNT" forVirtualAddress:0x04000006 reason:NCReason_Import];
    [file setName:@"BG0CNT" forVirtualAddress:0x04000008 reason:NCReason_Import];
    [file setName:@"BG1CNT" forVirtualAddress:0x0400000A reason:NCReason_Import];
    [file setName:@"BG2CNT" forVirtualAddress:0x0400000C reason:NCReason_Import];
    [file setName:@"BG3CNT" forVirtualAddress:0x0400000E reason:NCReason_Import];
    [file setName:@"BG0HOFS" forVirtualAddress:0x04000010 reason:NCReason_Import];
    [file setName:@"BG0VOFS" forVirtualAddress:0x04000012 reason:NCReason_Import];
    [file setName:@"BG1HOFS" forVirtualAddress:0x04000014 reason:NCReason_Import];
    [file setName:@"BG1VOFS" forVirtualAddress:0x04000016 reason:NCReason_Import];
    [file setName:@"BG2HOFS" forVirtualAddress:0x04000018 reason:NCReason_Import];
    [file setName:@"BG2VOFS" forVirtualAddress:0x0400001a reason:NCReason_Import];
    [file setName:@"BG3HOFS" forVirtualAddress:0x0400001c reason:NCReason_Import];
    [file setName:@"BG3VOFS" forVirtualAddress:0x0400001e reason:NCReason_Import];
    [file setName:@"BG2PA" forVirtualAddress:0x04000020 reason:NCReason_Import];
    [file setName:@"BG2PB" forVirtualAddress:0x04000022 reason:NCReason_Import];
    [file setName:@"BG2PC" forVirtualAddress:0x04000024 reason:NCReason_Import];
    [file setName:@"BG2PD" forVirtualAddress:0x04000026 reason:NCReason_Import];
    [file setName:@"BG2X" forVirtualAddress:0x04000028 reason:NCReason_Import];
    [file setName:@"BG2Y" forVirtualAddress:0x0400002C reason:NCReason_Import];
    [file setName:@"BG3PA" forVirtualAddress:0x04000030 reason:NCReason_Import];
    [file setName:@"BG3PB" forVirtualAddress:0x04000032 reason:NCReason_Import];
    [file setName:@"BG3PC" forVirtualAddress:0x04000034 reason:NCReason_Import];
    [file setName:@"BG3PD" forVirtualAddress:0x04000036 reason:NCReason_Import];
    [file setName:@"BG3X" forVirtualAddress:0x04000038 reason:NCReason_Import];
    [file setName:@"BG3Y" forVirtualAddress:0x0400003C reason:NCReason_Import];
    [file setName:@"WIN0H" forVirtualAddress:0x04000040 reason:NCReason_Import];
    [file setName:@"WIN1H" forVirtualAddress:0x04000042 reason:NCReason_Import];
    [file setName:@"WIN0V" forVirtualAddress:0x04000044 reason:NCReason_Import];
    [file setName:@"WIN1V" forVirtualAddress:0x04000046 reason:NCReason_Import];
    [file setName:@"WININ" forVirtualAddress:0x04000048 reason:NCReason_Import];
    [file setName:@"WINOUT" forVirtualAddress:0x0400004a reason:NCReason_Import];
    [file setName:@"MOSAIC" forVirtualAddress:0x0400004C reason:NCReason_Import];
    [file setName:@"BLDCNT" forVirtualAddress:0x04000050 reason:NCReason_Import];
    [file setName:@"BLDALPHA" forVirtualAddress:0x04000052 reason:NCReason_Import];
    [file setName:@"BLDY" forVirtualAddress:0x04000054 reason:NCReason_Import];
    [file setName:@"SOUND1CNT_L" forVirtualAddress:0x04000060 reason:NCReason_Import];
    [file setName:@"SOUND1CNT_H" forVirtualAddress:0x04000062 reason:NCReason_Import];
    [file setName:@"SOUND1CNT_X" forVirtualAddress:0x04000064 reason:NCReason_Import];
    [file setName:@"SOUND2CNT_L" forVirtualAddress:0x04000068 reason:NCReason_Import];
    [file setName:@"SOUND2CNT_H" forVirtualAddress:0x0400006c reason:NCReason_Import];
    [file setName:@"SOUND3CNT_L" forVirtualAddress:0x04000070 reason:NCReason_Import];
    [file setName:@"SOUND3CNT_H" forVirtualAddress:0x04000072 reason:NCReason_Import];
    [file setName:@"SOUND3CNT_X" forVirtualAddress:0x04000074 reason:NCReason_Import];
    [file setName:@"SOUND4CNT_L" forVirtualAddress:0x04000078 reason:NCReason_Import];
    [file setName:@"SOUND4CNT_H" forVirtualAddress:0x0400007C reason:NCReason_Import];
    [file setName:@"SOUNDCNT_L" forVirtualAddress:0x04000080 reason:NCReason_Import];
    [file setName:@"SOUNDCNT_H" forVirtualAddress:0x04000082 reason:NCReason_Import];
    [file setName:@"SOUNDCNT_X" forVirtualAddress:0x04000084 reason:NCReason_Import];
    [file setName:@"SOUNDBIAS" forVirtualAddress:0x04000088 reason:NCReason_Import];
    [file setName:@"WAVE_RAM" forVirtualAddress:0x04000090 reason:NCReason_Import];
    [file setName:@"FIFO_A" forVirtualAddress:0x040000A0 reason:NCReason_Import];
    [file setName:@"FIFO_B" forVirtualAddress:0x040000A4 reason:NCReason_Import];
    [file setName:@"DMA0SAD" forVirtualAddress:0x040000B0 reason:NCReason_Import];
    [file setName:@"DMA0DAD" forVirtualAddress:0x040000B4 reason:NCReason_Import];
    [file setName:@"DMA0CNT_L" forVirtualAddress:0x040000B8 reason:NCReason_Import];
    [file setName:@"DMA0CNT_H" forVirtualAddress:0x040000BA reason:NCReason_Import];
    [file setName:@"DMA1SAD" forVirtualAddress:0x040000BC reason:NCReason_Import];
    [file setName:@"DMA1DAD" forVirtualAddress:0x040000C0 reason:NCReason_Import];
    [file setName:@"DMA1CNT_L" forVirtualAddress:0x040000C4 reason:NCReason_Import];
    [file setName:@"DMA1CNT_H" forVirtualAddress:0x040000C6 reason:NCReason_Import];
    [file setName:@"DMA2SAD" forVirtualAddress:0x040000C8 reason:NCReason_Import];
    [file setName:@"DMA2DAD" forVirtualAddress:0x040000CC reason:NCReason_Import];
    [file setName:@"DMA2CNT_L" forVirtualAddress:0x040000D0 reason:NCReason_Import];
    [file setName:@"DMA2CNT_H" forVirtualAddress:0x040000D2 reason:NCReason_Import];
    [file setName:@"DMA3SAD" forVirtualAddress:0x040000D4 reason:NCReason_Import];
    [file setName:@"DMA3DAD" forVirtualAddress:0x040000D8 reason:NCReason_Import];
    [file setName:@"DMA3CNT_L" forVirtualAddress:0x040000DC reason:NCReason_Import];
    [file setName:@"DMA3CNT_H" forVirtualAddress:0x040000DE reason:NCReason_Import];
    [file setName:@"TM0CNT_L" forVirtualAddress:0x04000100 reason:NCReason_Import];
    [file setName:@"TM0CNT_H" forVirtualAddress:0x04000102 reason:NCReason_Import];
    [file setName:@"TM1CNT_L" forVirtualAddress:0x04000104 reason:NCReason_Import];
    [file setName:@"TM1CNT_H" forVirtualAddress:0x04000106 reason:NCReason_Import];
    [file setName:@"TM2CNT_L" forVirtualAddress:0x04000108 reason:NCReason_Import];
    [file setName:@"TM2CNT_H" forVirtualAddress:0x0400010a reason:NCReason_Import];
    [file setName:@"TM3CNT_L" forVirtualAddress:0x0400010c reason:NCReason_Import];
    [file setName:@"TM3CNT_H" forVirtualAddress:0x0400010e reason:NCReason_Import];
    [file setName:@"SIOMULTI0" forVirtualAddress:0x04000120 reason:NCReason_Import];
    [file setName:@"SIOMULTI1" forVirtualAddress:0x04000122 reason:NCReason_Import];
    [file setName:@"SIOMULTI2" forVirtualAddress:0x04000124 reason:NCReason_Import];
    [file setName:@"SIOMULTI3" forVirtualAddress:0x04000126 reason:NCReason_Import];
    [file setName:@"SIOCNT" forVirtualAddress:0x04000128 reason:NCReason_Import];
    [file setName:@"SIOMLT_SEND" forVirtualAddress:0x0400012A reason:NCReason_Import];
    [file setName:@"KEYINPUT" forVirtualAddress:0x04000130 reason:NCReason_Import];
    [file setName:@"KEYCNT" forVirtualAddress:0x04000132 reason:NCReason_Import];
    [file setName:@"RCNT" forVirtualAddress:0x04000134 reason:NCReason_Import];
    [file setName:@"IR" forVirtualAddress:0x04000136 reason:NCReason_Import];
    [file setName:@"JOYCNT" forVirtualAddress:0x04000140 reason:NCReason_Import];
    [file setName:@"JOY_RECV" forVirtualAddress:0x04000150 reason:NCReason_Import];
    [file setName:@"JOY_TRANS" forVirtualAddress:0x04000154 reason:NCReason_Import];
    [file setName:@"JOYSTAT" forVirtualAddress:0x04000158 reason:NCReason_Import];
    [file setName:@"IE" forVirtualAddress:0x04000200 reason:NCReason_Import];
    [file setName:@"IF" forVirtualAddress:0x04000202 reason:NCReason_Import];
    [file setName:@"WAITCNT" forVirtualAddress:0x04000204 reason:NCReason_Import];
    [file setName:@"IME" forVirtualAddress:0x04000208 reason:NCReason_Import];
    [file setName:@"POSTFLAG" forVirtualAddress:0x04000300 reason:NCReason_Import];
    [file setName:@"HALTCNT" forVirtualAddress:0x04000301 reason:NCReason_Import];

    [file setName:@"gba_bios_sound_buffer_ptr" forVirtualAddress:0x03007FF0 reason:NCReason_Import];
    [file setName:@"gba_bios_sound_buffer_ptr_mirror" forVirtualAddress:0x03FFFFF0 reason:NCReason_Import];
    [file setName:@"gba_bios_irq_flags" forVirtualAddress:0x03007FF8 reason:NCReason_Import];
    [file setName:@"gba_bios_irq_flags_mirror" forVirtualAddress:0x03FFFFF8 reason:NCReason_Import];
    [file setName:@"gba_bios_soft_reset_flag" forVirtualAddress:0x03007FFA reason:NCReason_Import];
    [file setName:@"gba_bios_soft_reset_flag_mirror" forVirtualAddress:0x03FFFFFA reason:NCReason_Import];
    [file setName:@"gba_bios_irq_handler" forVirtualAddress:0x03007FFC reason:NCReason_Import];
    [file setName:@"gba_bios_irq_handler_mirror" forVirtualAddress:0x03FFFFFC reason:NCReason_Import];

    [file setName:@"gba_header_logo" forVirtualAddress:0x08000004 reason:NCReason_Import];
    [file setType:Type_Int32 atVirtualAddress:0x08000004 forLength:0x080000A0-0x08000004];
    [file setName:@"gba_header_title" forVirtualAddress:0x080000A0 reason:NCReason_Import];
    [file setType:Type_ASCII atVirtualAddress:0x080000A0 forLength:12];
    [file setName:@"gba_header_gamecode" forVirtualAddress:0x080000AC reason:NCReason_Import];
    [file setType:Type_ASCII atVirtualAddress:0x080000AC forLength:4];
    [file setName:@"gba_header_makercode" forVirtualAddress:0x080000B0 reason:NCReason_Import];
    [file setType:Type_ASCII atVirtualAddress:0x080000B0 forLength:2];
    [file setName:@"gba_header_fixedvalue" forVirtualAddress:0x080000B2 reason:NCReason_Import];
    [file setType:Type_Int8 atVirtualAddress:0x080000B2 forLength:1];
    [file setName:@"gba_header_unitcode" forVirtualAddress:0x080000B3 reason:NCReason_Import];
    [file setType:Type_Int8 atVirtualAddress:0x080000B3 forLength:1];
    [file setName:@"gba_header_devicetype" forVirtualAddress:0x080000B4 reason:NCReason_Import];
    [file setType:Type_Int8 atVirtualAddress:0x080000B4 forLength:1];
    [file setType:Type_Int8 atVirtualAddress:0x080000B5 forLength:7];
    [file setName:@"gba_header_version" forVirtualAddress:0x080000BC reason:NCReason_Import];
    [file setType:Type_Int8 atVirtualAddress:0x080000BC forLength:1];
    [file setName:@"gba_header_checksum" forVirtualAddress:0x080000BD reason:NCReason_Import];
    [file setType:Type_Int8 atVirtualAddress:0x080000BD forLength:1];
    [file setType:Type_Int8 atVirtualAddress:0x080000BE forLength:2];

    callback(@"Loading GBA ROM File", 1.0f);
    file.cpuFamily = @"arm";
    file.cpuSubFamily = @"v6";
    file.addressSpaceWidthInBits = 32;
    file.integerWidthInBits = 32;

    [file addEntryPoint:firstAddress withCPUMode:0];

    return DIS_OK;
}

- (void)fixupRebasedFile:(NSObject<HPDisassembledFile> *)file withSlide:(int64_t)slide originalFileData:(NSData *)fileData {
    
}

- (FileLoaderLoadingStatus)loadDebugData:(NSData *)data forFile:(NSObject<HPDisassembledFile> *)file usingCallback:(FileLoadingCallbackInfo)callback {
    return DIS_NotSupported;
}

- (NSData *)extractFromData:(NSData *)data usingDetectedFileType:(NSObject<HPDetectedFileType> *)fileType returnAdjustOffset:(uint64_t *)adjustOffset returnAdjustFilename:(NSString *__autoreleasing *)newFilename {
    return nil;
}

@end
